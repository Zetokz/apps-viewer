package ua.zetokz.appsviewer.data;

import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;

/**
 * Created by Andrzej Mistetskij on 22.01.17 01:30
 */
public interface UserDataSource {

    /**
     * Saves result whether user is signed in
     *
     * @param isSignedIn
     */
    void setUserSignedIn(boolean isSignedIn);

    /**
     * Checks whether user has already signed in
     *
     * @return bool - whether user signed in
     */
    boolean isUserSignedIn();

    /**
     * Save device to lpcal storage
     *
     * @param deviceDTO Device data that will be saved
     */
    void saveDevice(DeviceDTO deviceDTO);

    /**
     * Save user credentials to local storage
     *
     * @param credentials Credentials
     */
    void saveAccount(AccountDTO credentials);

    /**
     * Load device data from local storage
     *
     * @return Device data
     */
    DeviceDTO getDevice();

    /**
     * Load account data from local storage
     *
     * @return Account data
     */
    AccountDTO getAccount();
}
