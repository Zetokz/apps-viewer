package ua.zetokz.appsviewer.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;

/**
 * Created by Andrzej Mistetskij on 22.01.17 01:31
 */
public class UserDataSourceImpl implements UserDataSource {

    private static final String PREF_NAME = "secured_pref";

    private static final String KEY_IS_USER_SIGNED_IN = "isUserSignedIn";
    private static final String KEY_ACCOUNT = "account";
    private static final String KEY_DEVICE = "device";

    private final Gson mGsonParser;
    private static SharedPreferences sPreferences;

    public UserDataSourceImpl(final Context context) {
        sPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mGsonParser = new GsonBuilder().create();
    }

    @Override
    public void setUserSignedIn(final boolean isSignedIn) {
        sPreferences.edit().putBoolean(KEY_IS_USER_SIGNED_IN, isSignedIn).apply();
    }

    @Override
    public boolean isUserSignedIn() {
        return sPreferences.getBoolean(KEY_IS_USER_SIGNED_IN, false);
    }

    @Override
    public void saveAccount(final AccountDTO credentials) {
        final String credentialsJson = mGsonParser.toJson(credentials);
        //unsecured!!! need to be encrypted in future
        sPreferences.edit().putString(KEY_ACCOUNT, credentialsJson).apply();
    }

    @Override
    public void saveDevice(final DeviceDTO device) {
        final String rawDevice = mGsonParser.toJson(device);
        sPreferences.edit().putString(KEY_DEVICE, rawDevice).apply();
    }

    @Override
    public AccountDTO getAccount() {
        final String rawAccount = sPreferences.getString(KEY_ACCOUNT, null);
        return mGsonParser.fromJson(rawAccount, AccountDTO.class);
    }

    @Override
    public DeviceDTO getDevice() {
        final String rawAccount = sPreferences.getString(KEY_DEVICE, null);
        return mGsonParser.fromJson(rawAccount, DeviceDTO.class);
    }
}
