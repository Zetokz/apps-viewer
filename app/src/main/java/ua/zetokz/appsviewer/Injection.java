package ua.zetokz.appsviewer;

import android.content.Context;

import ua.zetokz.appsviewer.data.UserDataSource;
import ua.zetokz.appsviewer.data.UserDataSourceImpl;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractor;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractorImpl;
import ua.zetokz.appsviewer.manager.ApplicationsManager;
import ua.zetokz.appsviewer.util.schedulers.BaseSchedulerProvider;
import ua.zetokz.appsviewer.util.schedulers.SchedulerProvider;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:41
 */
public class Injection {
    private Injection() {
        throw new IllegalAccessError("Injection class");
    }

    /**
     * Provide scheduler provider
     *
     * @return SchedulerProvider
     */
    public static BaseSchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }

    /**
     * Provide profile interactor implementation
     *
     * @return ProfileInteractor
     */
    public static AppViewerInteractor provideAppViewInteractor() {
        return new AppViewerInteractorImpl(provideSchedulerProvider().singleThread(), provideSchedulerProvider().ui());
    }

    /**
     * Provide active data source for access to user info.
     *
     * @param context {@link Context}
     * @return {@link ua.zetokz.appsviewer.data.UserDataSource}
     */
    public static UserDataSource provideUserDataSource(final Context context) {
        return new UserDataSourceImpl(context);
    }

    /**
     * Provide application manager.
     * @param context
     * @return
     */
    public static ApplicationsManager provideApplicationManager(final Context context) {
        return new ApplicationsManager(context);
    }
}
