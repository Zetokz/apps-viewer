package ua.zetokz.appsviewer;

import android.util.Log;

import java.io.IOException;

import ua.zetokz.appsviewer.domain.SettingsProvider;

import static android.content.ContentValues.TAG;

/**
 * Created by zetokz on 20.01.17.
 */

public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            SettingsProvider.loadConfiguration(getAssets().open("configuration.json"));
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }
}
