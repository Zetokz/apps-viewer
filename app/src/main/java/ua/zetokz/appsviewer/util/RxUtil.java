package ua.zetokz.appsviewer.util;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public abstract class RxUtil {

    private RxUtil() {
        throw new IllegalAccessError("RxUtil is utility class");
    }

    /**
     * Applying android main thread scheduler for observing call result.
     * Applying background (Schedulers.io()) for main work call.
     *
     * @param observable {@link Observable Observable for applying schedulers}
     * @param <T>        Observable type.
     * @return Observable with applied schedulers.
     */
    public static <T> Observable<T> applyMainThreadSchedulers(final Observable<T> observable) {
        return observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    /**
     * Cancel subscription if not null.
     *
     * @param subscription {@link Subscription Subscription for cancel}
     */
    public static void unsubscribeIfNotNull(final Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

}