package ua.zetokz.appsviewer.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.DisplayMetrics;

/**
 * Created by andrzejmistetskij on 21.01.17.
 */

public abstract class ResUtil {

    private ResUtil() {
        throw new IllegalAccessError("ResUtil is utility class");
    }

    /**
     * Wrap and set tint to drawable.
     *
     * @param viewDrawable Drawable for adding tint.
     * @param color        Tint color.
     * @return Tinted drawable.
     */
    public static Drawable setTint(final Drawable viewDrawable, final int color) {
        final Drawable drawable = DrawableCompat.wrap(viewDrawable);
        DrawableCompat.setTint(drawable, color);
        return drawable;
    }

    /**
     * Getting drawable from res and apply tint for this drawable.
     *
     * @param context     Context for accessing drawable resources.
     * @param drawableRes Drawable resource id.
     * @param color       Tint color.
     * @return Tinted drawable.
     */
    public static Drawable getDrawableWithTint(final Context context, final @DrawableRes int drawableRes, final int color) {
        return setTint(ContextCompat.getDrawable(context, drawableRes), color);
    }

    /**
     * Getting string from resources
     *
     * @param context   Context for accessing string resources.
     * @param stringRes String resource id.
     * @return Text from resources
     */
    public static String getString(final Context context, final @StringRes int stringRes) {
        return context.getString(stringRes);
    }

    /**
     * Getting color from resources
     *
     * @param context      Context for accessing color resources
     * @param textColorRes Color resource id.
     * @return Color Int.
     */
    public static int getColor(final Context context, final int textColorRes) {
        return ContextCompat.getColor(context, textColorRes);
    }

    /**
     * Convert value to dimensions
     *
     * @param context Context for accessing resources
     * @param dp      integer value that should be converted
     * @return Integer value in pixels that complies dp count
     */
    public static int convertDpToPx(final Context context, final int dp) {
        final Resources resources = context.getResources();
        final DisplayMetrics metrics = resources.getDisplayMetrics();
        final float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    /**
     * Getting integer value from resources
     *
     * @param context Context for accessing resources
     * @param resId   Integer resId
     * @return Integer value
     */
    public static int getInteger(final Context context, final @IntegerRes int resId) {
        return context.getResources().getInteger(resId);
    }
}