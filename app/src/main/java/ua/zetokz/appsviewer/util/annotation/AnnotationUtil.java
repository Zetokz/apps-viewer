package ua.zetokz.appsviewer.util.annotation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Annotation;

/**
 * Created by Yevhenii Rechun on 23.01.17 05:07.
 */
public class AnnotationUtil {

    public static <A extends Annotation> A getClassAnnotation(@Nullable final Class<?> clazz, @NonNull final Class<A> annotation) {
        if (clazz == null) return null;
        if (clazz.isAnnotationPresent(annotation)) {
            return clazz.getAnnotation(annotation);
        } else {
            return null;
        }
    }

    public static <A extends Annotation> A getClassAnnotation(@Nullable final Object obj, @NonNull final Class<A> annotation) {
        if (obj == null) return null;
        final Class<?> aClass = obj.getClass();
        return getClassAnnotation(aClass, annotation);
    }
}
