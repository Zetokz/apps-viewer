package ua.zetokz.appsviewer.util;

import android.os.Build;

import java.util.UUID;

import ua.zetokz.appsviewer.domain.dto.DeviceDTO;

/**
 * Created by Andrzej Mistetskij on 22.01.17 13:54
 */
public class DeviceUtil {
    private DeviceUtil() {
        throw new IllegalAccessError("DeviceUtil is utility class");
    }

    /**
     * Returns unique device id.
     *
     * @return
     */
    public static String getUniqueDeviceId() {
        String uniqueDeviceId = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);
        String serial = null;
        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();

            // Return the serial for api => 9
            return "appsviewer-" + new UUID(uniqueDeviceId.hashCode(), serial.hashCode()).toString();
        } catch (Exception exception) {
            serial = "serial";
        }
        return "appsviewer-" + new UUID(uniqueDeviceId.hashCode(), serial.hashCode()).toString();
    }

    /**
     * Returns device name based on device brand and android version
     *
     * @return string - device name
     */
    public static String getDeviceName() {
        return Build.BRAND + " " + Build.VERSION.RELEASE;
    }

    /**
     * @return instance of current device
     */
    public static DeviceDTO createCurrentDevice() {
        DeviceDTO deviceDTO = new DeviceDTO();
        deviceDTO.setName(getDeviceName());
        deviceDTO.setIdentifier(getUniqueDeviceId());
        return deviceDTO;
    }
}
