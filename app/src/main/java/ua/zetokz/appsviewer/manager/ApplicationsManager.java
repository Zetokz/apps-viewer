package ua.zetokz.appsviewer.manager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import java.io.File;
import java.util.List;

import rx.Observable;
import ua.zetokz.appsviewer.presentation.model.AppInfo;

/**
 * Created by Yevhenii Rechun on 21.01.17 02:20
 */
public class ApplicationsManager {

    private static final String TAG = ApplicationsManager.class.getSimpleName();

    private final Context mContext;
    private final PackageManager mPackageManager;

    public ApplicationsManager(final Context context) {
        mContext = context;
        mPackageManager = context.getPackageManager();
    }

    public Observable<List<AppInfo>> getApplicationList() {
        final PackageManager packageManager = mContext.getPackageManager();

        final List<PackageInfo> packages = packageManager.getInstalledPackages(0);

        return Observable.from(packages)
                .flatMap(this::convertRawData)
                .toSortedList((appInfo, appInfo2) -> appInfo.getAppName().compareTo(appInfo2.getAppName()));
    }

    public AppInfo getAppInfoByPackageName(final String packageName) {
        try {
            final PackageInfo packageInfo = mPackageManager.getPackageInfo(packageName, 0);
            return convertRawData(packageInfo).toBlocking().first();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Observable<AppInfo> convertRawData(final PackageInfo packageInfo) {
        final ApplicationInfo applicationInfo = packageInfo.applicationInfo;

        final String applicationName = mPackageManager.getApplicationLabel(applicationInfo).toString();
        final float applicationSize = calculateSize(applicationInfo);
        final Drawable applicationIcon = mPackageManager.getApplicationIcon(applicationInfo);
        final String applicationPackage = applicationInfo.packageName;


        final AppInfo appInfo = new AppInfo.Builder()
                .appName(applicationName)
                .packageName(applicationPackage)
                .icon(applicationIcon)
                .size(applicationSize)
                .installedDate(packageInfo.firstInstallTime)
                .updatedDate(packageInfo.lastUpdateTime)
                .build();
        return Observable.just(appInfo);
    }

    private float calculateSize(final ApplicationInfo applicationInfo) {
        final File file = new File(applicationInfo.sourceDir);
        final float size = file.length() / 1024 / 1024;
        return size;
    }

    public Intent createIntentForOpeningApp(final AppInfo appInfo) {
        return mPackageManager.getLaunchIntentForPackage(appInfo.getPackageName());
    }

    public Intent createIntentForDeletingApp(final AppInfo appInfo) {
        final Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + appInfo.getPackageName()));
        return intent;
    }
}
