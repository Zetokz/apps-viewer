package ua.zetokz.appsviewer.presentation;

import android.content.Context;

/**
 * Created by zetokz on 20.01.17.
 */

interface ContextProvider {

    Context getContext();
}
