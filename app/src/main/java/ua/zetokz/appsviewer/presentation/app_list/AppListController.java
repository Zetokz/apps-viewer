package ua.zetokz.appsviewer.presentation.app_list;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bluelinelabs.conductor.RouterTransaction;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import ua.zetokz.appsviewer.Injection;
import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.adapter.ListAdapterDelegate;
import ua.zetokz.appsviewer.adapter.delegate.AppSingleItemDelegate;
import ua.zetokz.appsviewer.manager.ApplicationsManager;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.model.ListItem;
import ua.zetokz.appsviewer.presentation.BaseController;
import ua.zetokz.appsviewer.presentation.app_details.AppDetailsController;
import ua.zetokz.appsviewer.presentation.model.ServerApp;
import ua.zetokz.appsviewer.presentation.sign_in.SignInController;
import ua.zetokz.appsviewer.util.BundleBuilder;
import ua.zetokz.appsviewer.util.transitionanimation.CircularRevealChangeHandlerCompat;
import ua.zetokz.appsviewer.util.ResUtil;

import static ua.zetokz.appsviewer.Injection.provideAppViewInteractor;
import static ua.zetokz.appsviewer.Injection.provideApplicationManager;
import static ua.zetokz.appsviewer.Injection.provideUserDataSource;

public class AppListController extends BaseController implements AppListContract.View {

    private static final String KEY_IS_OFFLINE_MODE_ENABLED = "isOfflineModeEnabled";

    private View mContainerView;
    private RecyclerView mRecycler;
    private ProgressBar mProgressBar;
    private ListAdapterDelegate mAdapter;
    private AppListContract.Presenter mPresenter;

    public AppListController() {
    }

    public AppListController(final boolean isOfflineMode) {
        super(new BundleBuilder(new Bundle())
                .putBoolean(KEY_IS_OFFLINE_MODE_ENABLED, isOfflineMode)
                .build());
    }

    @Override
    protected void onViewBound(@NonNull final View rootView) {
        super.onViewBound(rootView);
        setHasOptionsMenu(true);
        setUpActionBar();
        setUpViews(rootView);
        mAdapter = createConfiguredAdapter();

        final boolean isOfflineMode = getArgs().getBoolean(KEY_IS_OFFLINE_MODE_ENABLED, false);
        mPresenter = new AppListPresenter(this, provideAppViewInteractor(), provideApplicationManager(getContext()),
                provideUserDataSource(getContext()), isOfflineMode);
    }

    @Override
    protected void onAttach(@NonNull final View view) {
        super.onAttach(view);
        mPresenter.start();
    }

    @Override
    protected void onDestroyView(final View view) {
        super.onDestroyView(view);
        mPresenter.stop();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.controller_app_list;
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showAppList(final List<AppInfo> apps) {
        mRecycler.setVisibility(View.VISIBLE);
        mRecycler.setAdapter(mAdapter);
        mAdapter.addTailAllItems(apps);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void openAppDetailsController(final String packageName) {
        getRouter().pushController(RouterTransaction.with(new AppDetailsController(packageName)));
    }

    @Override
    public void openApp(final Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showAppCantBeOpened() {
        Toast.makeText(getContext(), R.string.app_cant_be_opened, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessageGoOnline() {
        Snackbar.make(mContainerView, R.string.not_available_in_offline_mode, Snackbar.LENGTH_LONG)
                .setAction(R.string.sign_in_snackbar_action, view -> mPresenter.openSignInController(view))
                .show();
    }

    @Override
    public void openDrawer() {
        getDrawerLayout().openDrawer(GravityCompat.START);
    }

    @Override
    public void navigateToSignInController(final View view) {
        getRouter().replaceTopController(RouterTransaction.with(new SignInController())
                .pushChangeHandler(new CircularRevealChangeHandlerCompat(view, mContainerView))
                .popChangeHandler(new CircularRevealChangeHandlerCompat(view, mContainerView)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mPresenter.onOpenDrawerClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpViews(final @NonNull View rootView) {
        mContainerView = rootView.findViewById(R.id.flContainer);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.pbProgress);
        mRecycler = (RecyclerView) rootView.findViewById(R.id.rvAppList);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mProgressBar.getIndeterminateDrawable().setColorFilter(ResUtil.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    private void setUpActionBar() {
        showActionBar();
        setTitle(R.string.app_list_controller_title);
        final ActionBar actionBar = getActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private ListAdapterDelegate createConfiguredAdapter() {
        final ArrayList<AdapterDelegate<List<ListItem>>> delegates = new ArrayList<>();
        delegates.add(new AppSingleItemDelegate(getContext(),
                (position, appItem) -> mPresenter.onAppDetailsClicked(appItem),
                (position, appItem) -> mPresenter.onOpenAppClicked(appItem)));
        return new ListAdapterDelegate(delegates, null);
    }
}
