package ua.zetokz.appsviewer.presentation.app_list;

import android.content.Intent;

import java.util.List;

import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.BasePresenter;
import ua.zetokz.appsviewer.presentation.BaseView;

/**
 * Created by andrzejmistetskij on 21.01.17.
 */

public interface AppListContract {

    interface View extends BaseView<Presenter> {

        /**
         * Shows progress bar
         */
        void showProgress();

        /**
         * Hides progress bar
         */
        void hideProgress();

        /**
         * Shows applications' list
         *
         * @param apps
         */
        void showAppList(final List<AppInfo> apps);

        /**
         * Navigates to {@link ua.zetokz.appsviewer.presentation.app_details.AppDetailsController}
         *
         * @param packageName
         */
        void openAppDetailsController(String packageName);

        /**
         * Open intent
         *
         * @param intent Intent that will be opened if possible
         */
        void openApp(Intent intent);

        /**
         * SHow app cant be opened message
         */
        void showAppCantBeOpened();

        /**
         * Navigate to sign in controller
         *
         * @param view Clicked view
         */
        void navigateToSignInController(final android.view.View view);

        /**
         * Show message for redirect to online mode
         */
        void showMessageGoOnline();

        /**
         * Open drawer
         */
        void openDrawer();
    }

    interface Presenter extends BasePresenter {

        /**
         * Retrieves all installed apps on device
         */
        void loadInstalledApps();

        /**
         * Click listener on open app
         */
        void onOpenAppClicked(AppInfo appInfo);

        /**
         * Destroy presenter
         */
        void stop();

        /**
         * Click listener for app details
         *
         * @param appInfo App data
         */
        void onAppDetailsClicked(AppInfo appInfo);

        /**
         * Click listener for open drawer
         */
        void onOpenDrawerClicked();

        /**
         * Click listener for open sign in controller
         *
         * @param view View that was clicked
         */
        void openSignInController(android.view.View view);
    }
}
