package ua.zetokz.appsviewer.presentation;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.bluelinelabs.conductor.Controller;

import ua.zetokz.appsviewer.util.ResUtil;

public abstract class BaseController extends Controller {

    private boolean mActive = false;

    public BaseController() {
    }

    public BaseController(Bundle args) {
        super(args);
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull final LayoutInflater inflater, @NonNull final ViewGroup container) {
        final View rootView = inflater.inflate(getLayoutRes(), container, false);
        onViewBound(rootView);
        return rootView;
    }

    protected void onViewBound(@NonNull final View rootView) {

    }

    protected ActionBar getActionBar() {
        ActionBarProvider actionBarProvider = ((ActionBarProvider) getActivity());
        return actionBarProvider.getSupportActionBar();
    }

    protected void showActionBar() {
        getActionBar().show();
    }

    protected void hideActionBar() {
        getActionBar().hide();
    }

    protected void setActionBar(Toolbar toolbar) {
        ActionBarProvider actionBarProvider = ((ActionBarProvider) getActivity());
        actionBarProvider.setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Set the default behavior to be back navigation.
                getRouter().handleBack();
                return true;
        }
        return false;
    }

    @Override
    protected void onDestroyView(View view) {
        super.onDestroyView(view);
        setActive(false);
        // Note: in a real application you may wish to unbind your view references by
        // overriding this method and setting each reference to null. This releases the Views
        // that are on the back-stack and saves memory.
    }

    protected void setTitle(final String title) {
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) actionBar.setTitle(title);
    }

    protected void setTitle(final @StringRes int titleRes) {
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) actionBar.setTitle(titleRes);
    }

    protected void setActive(boolean active) {
        mActive = active;
    }

    public boolean isActive() {
        return mActive;
    }

    protected void setStatusBarColor(@ColorRes final int statusBarColorRes) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final Window window = getActivity().getWindow();
            if (window != null) window.setStatusBarColor(ResUtil.getColor(getContext(), statusBarColorRes));
        }
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected Context getContext() {
        ContextProvider contextProvider = ((ContextProvider) getActivity());
        return contextProvider != null ? contextProvider.getContext() : null;
    }

    protected DrawerLayout getDrawerLayout() {
        DrawerLayoutProvider drawerLayoutProvider = ((DrawerLayoutProvider) getActivity());
        return drawerLayoutProvider.getDrawerLayout();
    }
}