package ua.zetokz.appsviewer.presentation.app_details;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ua.zetokz.appsviewer.Injection;
import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.interfaces.LockDrawer;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.BaseController;
import ua.zetokz.appsviewer.util.BundleBuilder;

/**
 * Created by zetokz on 20.01.17.
 */

@LockDrawer
public class AppDetailsController extends BaseController implements AppDetailsContract.View {

    private static final String KEY_PACKAGE_NAME = "packageName";

    private ImageView mAppAvatar;
    private TextView mAppName;
    private TextView mAppPackageName;
    private TextView mAppSize;
    private AppDetailsContract.Presenter mPresenter;

    public AppDetailsController(final String packageName) {
        this(new BundleBuilder(new Bundle()).putString(KEY_PACKAGE_NAME, packageName)
                .build());
    }

    public AppDetailsController(final Bundle args) {
        super(args);
    }

    @Override
    protected void onViewBound(@NonNull final View rootView) {
        super.onViewBound(rootView);
        setHasOptionsMenu(true);
        setUpActionBar();
        setUpViews(rootView);
        mPresenter = new AppDetailsPresenter(this, Injection.provideApplicationManager(getContext()), getArgs().getString(KEY_PACKAGE_NAME));
    }

    @Override
    protected void onAttach(@NonNull final View view) {
        super.onAttach(view);
        mPresenter.start();
    }

    @Override
    public void showAppInfo(final AppInfo appInfo) {
        setAppInfo(appInfo);
        setTitle(appInfo.getAppName());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull final Menu menu, @NonNull final MenuInflater inflater) {
        inflater.inflate(R.menu.app_details_menu, menu);
        menu.findItem(R.id.menu_open_app).setOnMenuItemClickListener(__ -> {
            mPresenter.onOpenAppMenuItemClicked();
            return true;
        });
        menu.findItem(R.id.menu_delete_app).setOnMenuItemClickListener(__ -> {
            mPresenter.onDeleteAppMenuItemClicker();
            return true;
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.controller_app_details;
    }

    @Override
    public void openApp(final Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showAppCantBeOpened() {
        Toast.makeText(getContext(), R.string.app_cant_be_opened, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deleteApp(final Intent intent) {
        startActivity(intent);
    }

    private void setUpActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setUpViews(final View rootView) {
        mAppAvatar = (ImageView) rootView.findViewById(R.id.ivAppAvatar);
        mAppName = (TextView) rootView.findViewById(R.id.tvAppName);
        mAppPackageName = (TextView) rootView.findViewById(R.id.tvPackageName);
        mAppSize = (TextView) rootView.findViewById(R.id.tvSize);
    }

    private void setAppInfo(final AppInfo info) {
        mAppAvatar.setImageDrawable(info.getIcon());
        mAppName.setText(info.getAppName());
        mAppPackageName.setText(info.getPackageName());
        mAppSize.setText(info.getApplicationSize());
    }
}
