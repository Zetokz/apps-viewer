package ua.zetokz.appsviewer.presentation.app_list;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.List;

import rx.subscriptions.CompositeSubscription;
import ua.zetokz.appsviewer.data.UserDataSource;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractor;
import ua.zetokz.appsviewer.manager.ApplicationsManager;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.util.RxUtil;

/**
 * Created by Andrzej Mistetskij on 21.01.17 02:22
 */
final class AppListPresenter implements AppListContract.Presenter {

    private final CompositeSubscription mSubscriptions;
    private final AppListContract.View mView;
    private final ApplicationsManager mAppsManager;
    private final AppViewerInteractor mAppViewerInteractor;
    private final UserDataSource mDataSource;
    private final boolean isOfflineModeEnabled;

    AppListPresenter(@NonNull final AppListContract.View view, @NonNull AppViewerInteractor interactor,
                     @NonNull final ApplicationsManager appsManager, @NonNull final UserDataSource dataSource,
                     final boolean isOfflineModeEnabled) {

        mDataSource = dataSource;
        mSubscriptions = new CompositeSubscription();
        mView = view;
        mAppsManager = appsManager;
        this.isOfflineModeEnabled = isOfflineModeEnabled;
        mAppViewerInteractor = interactor;
    }

    @Override
    public void loadInstalledApps() {
        mView.showProgress();
        mSubscriptions.add(RxUtil.applyMainThreadSchedulers(mAppsManager.getApplicationList())
                .subscribe(this::onApplicationLoadSuccess, throwable -> {
                    // TODO: 23.01.17 create Subject for publish success action for avoid too long chain of rx calls
                }));
    }

    @Override
    public void onOpenAppClicked(final AppInfo appInfo) {
        final Intent intent = mAppsManager.createIntentForOpeningApp(appInfo);
        if (intent == null) {
            mView.showAppCantBeOpened();
        } else {
            mView.openApp(intent);
        }
    }

    @Override
    public void start() {
        mView.showProgress();
        loadInstalledApps();
    }

    @Override
    public void stop() {
        RxUtil.unsubscribeIfNotNull(mSubscriptions);
    }

    @Override
    public void onAppDetailsClicked(final AppInfo appInfo) {
        mView.openAppDetailsController(appInfo.getPackageName());
    }

    @Override
    public void onOpenDrawerClicked() {
        if (isOfflineModeEnabled) {
            mView.showMessageGoOnline();
        } else {
            mView.openDrawer();
        }
    }

    @Override
    public void openSignInController(final View view) {
        mView.navigateToSignInController(view);
    }

    private void onApplicationLoadSuccess(final List<AppInfo> apps) {
        mView.hideProgress();
        mView.showAppList(apps);

        if (!isOfflineModeEnabled) sendAppsToServer(apps);
    }

    private void sendAppsToServer(final List<AppInfo> apps) {
        //todo device.getIdentifier() should be changed in future to device.getId() when server will be updated
        final DeviceDTO device = mDataSource.getDevice();
        mAppViewerInteractor.uploadApplications(mDataSource.getAccount(), device.getIdentifier(), apps).execute();
    }
}
