package ua.zetokz.appsviewer.presentation.app_details;

import android.content.Intent;

import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.BasePresenter;
import ua.zetokz.appsviewer.presentation.BaseView;

/**
 * Created by Andrzej Mistetskij on 22.01.17 17:57
 */
public interface AppDetailsContract {

    interface View extends BaseView<Presenter> {

        void showAppInfo(AppInfo appInfo);

        void openApp(Intent intent);

        void showAppCantBeOpened();

        void deleteApp(Intent intent);
    }

    interface Presenter extends BasePresenter {

        void onOpenAppMenuItemClicked();

        void onDeleteAppMenuItemClicker();
    }
}
