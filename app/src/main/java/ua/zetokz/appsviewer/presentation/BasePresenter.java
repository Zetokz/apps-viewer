package ua.zetokz.appsviewer.presentation;

public interface BasePresenter {

    /**
     * Start business logic here
     * Call in onAttachedView
     */
    void start();

}