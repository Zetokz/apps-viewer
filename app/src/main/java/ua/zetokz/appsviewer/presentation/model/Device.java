package ua.zetokz.appsviewer.presentation.model;

/**
 * Created by Andrzej Mistetskij on 23.01.17 01:19
 */
public class Device implements ListItem {

    private String name;
    private String identifier;
    private boolean isSelected;

    public Device(final String name, final String identifier) {
        this.name = name;
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }
}
