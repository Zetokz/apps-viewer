package ua.zetokz.appsviewer.presentation.device_app_list;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import ua.zetokz.appsviewer.Injection;
import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.adapter.ListAdapterDelegate;
import ua.zetokz.appsviewer.adapter.delegate.RemoteDeviceAppItemDelegate;
import ua.zetokz.appsviewer.presentation.BaseController;
import ua.zetokz.appsviewer.presentation.interfaces.LockDrawer;
import ua.zetokz.appsviewer.presentation.model.ListItem;
import ua.zetokz.appsviewer.presentation.model.ServerApp;
import ua.zetokz.appsviewer.util.BundleBuilder;
import ua.zetokz.appsviewer.util.ResUtil;

/**
 * Created by Yevhenii Rechun on 23.01.17 02:11.
 */
@LockDrawer
public class SingleDeviceAppListController extends BaseController implements SingleDeviceAppListContract.View {

    private static final String ARG_DEVICE_NAME = "arg_device_name";
    private static final String ARG_DEVICE_IDENTIFIER = "arg_device_identifier";

    private RecyclerView mRecycler;
    private ProgressBar mProgressBar;
    private ListAdapterDelegate mAdapter;
    private SingleDeviceAppListContract.Presenter mPresenter;

    public SingleDeviceAppListController() {
    }

    public SingleDeviceAppListController(final String deviceName, final String deviceIdentifier) {
        super(new BundleBuilder(new Bundle())
                .putString(ARG_DEVICE_NAME, deviceName).putString(ARG_DEVICE_IDENTIFIER, deviceIdentifier)
                .build());
    }

    protected void onViewBound(@NonNull final View rootView) {
        super.onViewBound(rootView);
        setHasOptionsMenu(true);
        setUpActionBar();
        setUpViews(rootView);
        mAdapter = createConfiguredAdapter();
        mPresenter = createPresenter();
    }

    private SingleDeviceAppListContract.Presenter createPresenter() {
        return mPresenter == null ? new SingleDeviceAppListPresenter(
                this, Injection.provideAppViewInteractor(),
                Injection.provideUserDataSource(getContext()), getArgs().getString(ARG_DEVICE_IDENTIFIER)
        ) : mPresenter;
    }

    @Override
    protected void onAttach(@NonNull final View view) {
        super.onAttach(view);
        mPresenter.start();
    }

    @Override
    protected void onDestroyView(final View view) {
        super.onDestroyView(view);
        mPresenter.stop();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.controller_app_list;
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showAppList(final List<ServerApp> apps) {
        mRecycler.setVisibility(View.VISIBLE);
        mRecycler.setAdapter(mAdapter);
        mAdapter.addTailAllItems(apps);
        mAdapter.notifyDataSetChanged();
    }

    private void setUpActionBar() {
        showActionBar();
        setTitle(getArgs().getString(ARG_DEVICE_NAME));
        final ActionBar actionBar = getActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setUpViews(final @NonNull View rootView) {
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.pbProgress);
        mRecycler = (RecyclerView) rootView.findViewById(R.id.rvAppList);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mProgressBar.getIndeterminateDrawable().setColorFilter(ResUtil.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    private ListAdapterDelegate createConfiguredAdapter() {
        final ArrayList<AdapterDelegate<List<ListItem>>> delegates = new ArrayList<>();
        delegates.add(new RemoteDeviceAppItemDelegate(getContext()));
        return new ListAdapterDelegate(delegates, null);
    }

}
