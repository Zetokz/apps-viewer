package ua.zetokz.appsviewer.presentation.sign_in;

import ua.zetokz.appsviewer.presentation.BasePresenter;
import ua.zetokz.appsviewer.presentation.BaseView;

/**
 * Created by Andrzej Mistetskij on 21.01.17 14:47
 */
public interface SignInContract {

    interface View extends BaseView<Presenter> {

        /**
         * Shows progress to user
         */
        void showProgress();

        /**
         * Hides progress
         */
        void hideProgress();

        /**
         * Navigates to {@link ua.zetokz.appsviewer.presentation.app_list.AppListController}
         */
        void navigateToAppListController();

        /**
         * Navigates to {@link ua.zetokz.appsviewer.presentation.app_list.AppListController} and enables offline mode
         *
         * @param isOfflineModeEnabled
         */
        void navigateToAppListController(boolean isOfflineModeEnabled);

        /**
         * Shows message to user, for ex. u can use Toast or SnackBar to show user some info
         *
         * @param localizedMessage
         */
        void showMessage(final String localizedMessage);
    }

    interface Presenter extends BasePresenter {

        /**
         * Authenticates user
         *
         * @param name     username
         * @param password user' password
         */
        void authenticate(String name, String password);

        void stop();

        void onOfflineModeButtonClicked();
    }
}
