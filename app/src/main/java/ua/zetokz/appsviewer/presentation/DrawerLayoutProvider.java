package ua.zetokz.appsviewer.presentation;

import android.support.v4.widget.DrawerLayout;

public interface DrawerLayoutProvider {
    DrawerLayout getDrawerLayout();
}