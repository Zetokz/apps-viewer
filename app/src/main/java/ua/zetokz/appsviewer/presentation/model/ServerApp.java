package ua.zetokz.appsviewer.presentation.model;

/**
 * Created by Yevhenii Rechun on 23.01.17 01:09.
 */
public class ServerApp implements ListItem {

    private String name;
    private String packageName;
    private String icon;
    private long installedDate;
    private long updatedDate;
    private boolean deleted;

    public ServerApp() {}

    public ServerApp(final String name, final String packageName, final String icon, final long installedDate,
                     final long updatedDate, final boolean deleted) {

        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
        this.installedDate = installedDate;
        this.updatedDate = updatedDate;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getIcon() {
        return icon;
    }

    public long getInstalledDate() {
        return installedDate;
    }

    public long getUpdatedDate() {
        return updatedDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

}
