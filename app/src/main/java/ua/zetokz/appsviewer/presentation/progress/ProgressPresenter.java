package ua.zetokz.appsviewer.presentation.progress;

import android.support.annotation.NonNull;

/**
 * Created by Andrzej Mistetskij on 21.01.17 22:45
 */
public class ProgressPresenter implements ProgressContract.Presenter {

    private final ProgressContract.View mView;
    private final String mProgressText;

    /**
     * Create ProgressPresenter instance
     * @param view {@link ProgressContract.ua.zetokz.appsviewer.presentation.progress.ProgressContract.View}
     * @param progressText - text that will be displayed near progress bar if it was passed
     */
    public ProgressPresenter(@NonNull final ProgressContract.View view, final String progressText) {
        mView = view;
        mProgressText = progressText;
    }

    @Override
    public void start() {
        if (mProgressText != null) mView.showProgressText(mProgressText);
        else mView.setProgressTextVisibility(false);
    }
}
