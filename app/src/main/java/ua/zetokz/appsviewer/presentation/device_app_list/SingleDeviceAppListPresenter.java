package ua.zetokz.appsviewer.presentation.device_app_list;

import com.google.gson.Gson;

import java.util.List;

import rx.Observable;
import ua.zetokz.appsviewer.data.UserDataSource;
import ua.zetokz.appsviewer.domain.converter.DTOConverter;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractor;
import ua.zetokz.appsviewer.presentation.model.ServerApp;

/**
 * Created by Yevhenii Rechun on 23.01.17 02:11.
 */
public class SingleDeviceAppListPresenter implements SingleDeviceAppListContract.Presenter {

    private final AppViewerInteractor mInteractor;
    private final UserDataSource mDataSource;
    private final String mDeviceIdentifier;
    private final SingleDeviceAppListContract.View mView;
    private List<ServerApp> mCachedServerAppsUiList;

    public SingleDeviceAppListPresenter(final SingleDeviceAppListContract.View view, final AppViewerInteractor interactor,
                                        final UserDataSource dataSource, final String deviceIdentifier) {

        mView = view;
        mInteractor = interactor;
        mDataSource = dataSource;
        mDeviceIdentifier = deviceIdentifier;
    }

    @Override
    public void start() {
        loadRemoteDeviceApplications();
    }

    @Override
    public void stop() {
        mInteractor.unsubscribe();
    }

    @Override
    public void loadRemoteDeviceApplications() {
        if (mCachedServerAppsUiList != null) {
            mView.showAppList(mCachedServerAppsUiList);
            return;
        }
        final AccountDTO account = mDataSource.getAccount();
        mView.showProgress();
        mInteractor.loadDeviceApplications(account, mDeviceIdentifier)
                .execute(this::onApplicationsLoadedSuccess, this::onApplicationsLoadedError);
    }

    private void onApplicationsLoadedError(final Throwable throwable) {
        mView.hideProgress();
    }

    private void onApplicationsLoadedSuccess(final List<ApplicationDTO> applications) {
        mCachedServerAppsUiList = DTOConverter.convertApplicationDTOlistToServerAppList(applications); // // TODO: 23.01.17 refactor this using pattern Repository
        mView.hideProgress();
        mView.showAppList(mCachedServerAppsUiList);
    }
}
