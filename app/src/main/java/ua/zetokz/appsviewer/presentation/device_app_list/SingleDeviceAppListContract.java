package ua.zetokz.appsviewer.presentation.device_app_list;

import java.util.List;

import ua.zetokz.appsviewer.presentation.BasePresenter;
import ua.zetokz.appsviewer.presentation.BaseView;
import ua.zetokz.appsviewer.presentation.model.ServerApp;

/**
 * Created by Yevhenii Rechun on 23.01.17 02:11.
 */
public interface SingleDeviceAppListContract {

    interface View extends BaseView<Presenter> {

        void showProgress();

        void hideProgress();

        void showAppList(List<ServerApp> apps);
    }

    interface Presenter extends BasePresenter {

        void stop();

        void loadRemoteDeviceApplications();
    }
}
