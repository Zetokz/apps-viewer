package ua.zetokz.appsviewer.presentation.model;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;

/**
 * Created by Yevhenii Rechun on 21.01.17 13:26.
 */
public class AppInfo implements ListItem {

    private static final String SIZE_FORMAT = "%f MB";

    private final String appName;
    private final String packageName;
    private final String applicationSize;
    private final Drawable icon;
    private long installedDate;
    private long updatedDate;

    protected AppInfo(final String appName, final String packageName, final String applicationSize, final Drawable icon,
                      final long installedDate, final long updatedDate) {

        this.appName = appName;
        this.packageName = packageName;
        this.applicationSize = applicationSize;
        this.icon = icon;
        this.installedDate = installedDate;
        this.updatedDate = updatedDate;
    }

    public String getAppName() {
        return appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getApplicationSize() {
        return applicationSize;
    }

    public Drawable getIcon() {
        return icon;
    }

    public long getInstalledDate() {
        return installedDate;
    }

    public long getUpdatedDate() {
        return updatedDate;
    }

    public static class Builder {

        private String appName;
        private String packageName;
        private String size;
        private Drawable icon;
        private long installedDate;
        private long updatedDate;

        public Builder appName(final String appName) {
            this.appName = appName;
            return this;
        }

        public Builder packageName(final String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder size(final String size) {
            this.size = size;
            return this;
        }

        @SuppressLint("DefaultLocale")
        public Builder size(final float size) {
            this.size = String.format(AppInfo.SIZE_FORMAT, size);
            return this;
        }

        public Builder icon(final Drawable icon) {
            this.icon = icon;
            return this;
        }

        public Builder installedDate(final long installedDate) {
            this.installedDate = installedDate;
            return this;
        }

        public Builder updatedDate(final long updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public AppInfo build() {
            return new AppInfo(appName, packageName, size, icon, installedDate, updatedDate);
        }
    }
}
