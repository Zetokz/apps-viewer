package ua.zetokz.appsviewer.presentation.progress;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.BaseController;
import ua.zetokz.appsviewer.util.BundleBuilder;

/**
 * Created by Andrzej Mistetskij on 21.01.17 22:29
 */
public class ProgressController extends BaseController implements ProgressContract.View {

    private ProgressBar mProgressBar;
    private TextView mProgressText;
    private ProgressContract.Presenter mPresenter;

    private static final String KEY_TEXT = "ProgressText";

    public ProgressController() {
    }

    public ProgressController(final String progressText) {
        this(new BundleBuilder(new Bundle())
                .putString(KEY_TEXT, progressText)
                .build());
    }

    public ProgressController(final Bundle args) {
        super(args);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.controller_progress;
    }

    @Override
    protected void onViewBound(@NonNull final View rootView) {
        super.onViewBound(rootView);
        setUpViews(rootView);
        mPresenter = new ProgressPresenter(this, getArgs().getString(KEY_TEXT));
    }

    @Override
    protected void onAttach(@NonNull final View view) {
        super.onAttach(view);
        mPresenter.start();
    }

    @Override
    public void setProgressTextVisibility(final boolean visible) {
        mProgressText.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgressText(final String pregressText) {
        mProgressText.setText(pregressText);
    }

    private void setUpViews(final View rootView) {
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.pbProgress);
        mProgressText = (TextView) rootView.findViewById(R.id.tvProgressText);
    }
}
