package ua.zetokz.appsviewer.presentation.interfaces;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Yevhenii Rechun on 23.01.17 04:57.
 */
@Documented
@Retention(RUNTIME)
@Target({ElementType.TYPE})
public @interface LockDrawer {
}
