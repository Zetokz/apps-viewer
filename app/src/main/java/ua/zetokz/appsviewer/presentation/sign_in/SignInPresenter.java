package ua.zetokz.appsviewer.presentation.sign_in;

import android.support.annotation.NonNull;

import ua.zetokz.appsviewer.data.UserDataSource;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractor;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.dto.UploadDeviceDTO;

/**
 * Created by Andrzej Mistetskij on 21.01.17 14:48
 */
public class SignInPresenter implements SignInContract.Presenter {

    private final SignInContract.View mView;
    private final AppViewerInteractor mInteractor;
    private final UserDataSource mUserDataSource;
    private final UploadDeviceDTO mUploadDeviceDTO;
    private AccountDTO mCredentials;

    public SignInPresenter(@NonNull final SignInContract.View view, final AppViewerInteractor appViewerInteractor, final UserDataSource userDataSource) {
        mView = view;
        mInteractor = appViewerInteractor;
        mUserDataSource = userDataSource;
        mUploadDeviceDTO = new UploadDeviceDTO();
    }

    @Override
    public void start() {

    }

    @Override
    public void authenticate(final String name, final String password) {
        mView.showProgress();
        mInteractor.login(name, password).execute(this::onSignInSuccess, this::onSignInFailure);
    }

    @Override
    public void stop() {
        mInteractor.unsubscribe();
    }

    @Override
    public void onOfflineModeButtonClicked() {
        mView.navigateToAppListController(true);
    }

    private void onSignInSuccess(final AccountDTO account) {
        mCredentials = account;
        mUploadDeviceDTO.setCredentials(mCredentials);
        mInteractor.uploadDevice(mUploadDeviceDTO).execute(this::onDeviceAddedSuccessfully, this::onDeviceAddedError);
    }

    private void onSignInFailure(final Throwable throwable) {
        mView.hideProgress();
        mView.showMessage(throwable.getMessage());
    }

    private void onDeviceAddedSuccessfully(final DeviceDTO deviceDTO) {
        mUserDataSource.setUserSignedIn(true);
        mUserDataSource.saveAccount(mCredentials);
        mUserDataSource.saveDevice(deviceDTO);

        mView.hideProgress();
        mView.navigateToAppListController();
    }

    private void onDeviceAddedError(final Throwable throwable) {
        mView.hideProgress();
        mView.showMessage(throwable.getMessage());
    }
}
