package ua.zetokz.appsviewer.presentation.sign_in;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler;

import ua.zetokz.appsviewer.Injection;
import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.BaseController;
import ua.zetokz.appsviewer.presentation.app_list.AppListController;
import ua.zetokz.appsviewer.presentation.progress.ProgressController;
import ua.zetokz.appsviewer.util.ResUtil;

/**
 * Created by Andrzej Mistetskij on 21.01.17 14:43
 */
public class SignInController extends BaseController implements SignInContract.View {

    private SignInContract.Presenter mPresenter;
    private Button mSignInButton;
    private EditText mUsername;
    private EditText mPassword;
    private TextView mOfflineMode;
    private ViewGroup mOverlayRoot;
    private View mRootView;

    public SignInController() {
    }

    @Override
    protected void onViewBound(@NonNull final View rootView) {
        super.onViewBound(rootView);
        hideActionBar();
        setUpViews(rootView);
        setButtonClickListeners();

        mPresenter = new SignInPresenter(this, Injection.provideAppViewInteractor(), Injection.provideUserDataSource(getContext()));
    }

    @Override
    protected void onAttach(@NonNull final View view) {
        super.onAttach(view);
        mPresenter.start();
    }

    @Override
    protected void onDestroyView(final View view) {
        super.onDestroyView(view);
        mPresenter.stop();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.controller_signin;
    }

    @Override
    public void showProgress() {
        getChildRouter(mOverlayRoot)
                .setPopsLastView(true)
                .setRoot(RouterTransaction.with(new ProgressController(ResUtil.getString(getActivity(), R.string.progress_text_authenticating)))
                        .pushChangeHandler(new FadeChangeHandler())
                        .popChangeHandler(new FadeChangeHandler()));
    }

    @Override
    public void hideProgress() {
        getChildRouter(mOverlayRoot).popCurrentController();
    }

    @Override
    public void navigateToAppListController() {
        getRouter().replaceTopController(RouterTransaction.with(new AppListController())
                .pushChangeHandler(new FadeChangeHandler())
                .popChangeHandler(new FadeChangeHandler()));
    }

    @Override
    public void navigateToAppListController(final boolean isOfflineModeEnabled) {
        getRouter().replaceTopController(RouterTransaction.with(new AppListController(isOfflineModeEnabled))
                .pushChangeHandler(new FadeChangeHandler())
                .popChangeHandler(new FadeChangeHandler()));
    }

    @Override
    public void showMessage(final String localizedMessage) {
        Toast.makeText(getContext(), localizedMessage, Toast.LENGTH_SHORT).show();
    }

    private void setButtonClickListeners() {
        mSignInButton.setOnClickListener(__ -> mPresenter.authenticate(mUsername.getText().toString(), mPassword.getText().toString()));
        mOfflineMode.setOnClickListener(__ -> mPresenter.onOfflineModeButtonClicked());
    }

    private void setUpViews(final @NonNull View rootView) {
        mRootView = rootView.findViewById(R.id.flContainerView);
        mSignInButton = (Button) rootView.findViewById(R.id.btnSignIn);
        mUsername = (EditText) rootView.findViewById(R.id.etUsername);
        mPassword = (EditText) rootView.findViewById(R.id.etPassword);
        mOfflineMode = (TextView) rootView.findViewById(R.id.tvOfflineMode);
        mOverlayRoot = (ViewGroup) rootView.findViewById(R.id.flOverlayRoot);
    }
}
