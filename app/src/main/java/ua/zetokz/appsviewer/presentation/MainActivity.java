package ua.zetokz.appsviewer.presentation;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.ControllerChangeHandler;
import com.bluelinelabs.conductor.Router;
import com.bluelinelabs.conductor.RouterTransaction;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.ArrayList;
import java.util.List;

import ua.zetokz.appsviewer.Injection;
import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.adapter.ListAdapterDelegate;
import ua.zetokz.appsviewer.adapter.delegate.DeviceItemDelegate;
import ua.zetokz.appsviewer.adapter.delegate.EmptyDeviceItemDelegate;
import ua.zetokz.appsviewer.data.UserDataSource;
import ua.zetokz.appsviewer.domain.converter.DTOConverter;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.interactor.appviewer.AppViewerInteractor;
import ua.zetokz.appsviewer.presentation.app_list.AppListController;
import ua.zetokz.appsviewer.presentation.device_app_list.SingleDeviceAppListController;
import ua.zetokz.appsviewer.presentation.interfaces.LockDrawer;
import ua.zetokz.appsviewer.presentation.model.Device;
import ua.zetokz.appsviewer.presentation.model.EmptyDevice;
import ua.zetokz.appsviewer.presentation.model.ListItem;
import ua.zetokz.appsviewer.presentation.sign_in.SignInController;
import ua.zetokz.appsviewer.util.annotation.AnnotationUtil;

public class MainActivity extends AppCompatActivity implements ActionBarProvider, ContextProvider, DrawerLayoutProvider {

    private Router mRouter;
    private ViewGroup mContainer;
    private DrawerLayout mDrawerLayout;
    private RecyclerView mRecyclerView;
    private TextView mMyDevices;
    private ProgressBar mProgressBar;
    private ListAdapterDelegate mAdapter;
    private AppViewerInteractor mInteractor;
    private UserDataSource mDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUpViews();
        configureRecyclerViewInNavDrawer();
        setUpNavigationDrawer();
        mDataSource = Injection.provideUserDataSource(getContext());
        mInteractor = Injection.provideAppViewInteractor();
        mRouter = Conductor.attachRouter(this, mContainer, savedInstanceState);
        setRootController();
        setControllerChangeListener();
    }

    private void setControllerChangeListener() {
        mRouter.addChangeListener(new ControllerChangeHandler.ControllerChangeListener() {
            @Override
            public void onChangeStarted(@Nullable final Controller to, @Nullable final Controller from, final boolean isPush, @NonNull final ViewGroup container, @NonNull final ControllerChangeHandler handler) {
                if (from != null && from instanceof AppListController) mInteractor.unsubscribe();
            }

            @Override
            public void onChangeCompleted(@Nullable final Controller to, @Nullable final Controller from, final boolean isPush, @NonNull final ViewGroup container, @NonNull final ControllerChangeHandler handler) {
                if (to instanceof AppListController) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mInteractor.getDevices(mDataSource.getAccount()).execute(MainActivity.this::onDevicesLoadSuccess, MainActivity.this::onDevicesLoadError);
                }
                final LockDrawer annotation = AnnotationUtil.getClassAnnotation(to, LockDrawer.class);
                if (annotation != null) {
                    lockDrawer();
                } else {
                    unLockDrawer();
                }
            }
        });
    }

    private void setRootController() {
        if (!mRouter.hasRootController()) {
            mRouter.setRoot(RouterTransaction.with(mDataSource.isUserSignedIn() ? new AppListController() : new SignInController()));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInteractor.unsubscribe();
    }

    @Override
    public void onBackPressed() {
        if (!mRouter.handleBack()) {
            super.onBackPressed();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    private void setUpNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        mDrawerLayout.addDrawerListener(new AppViewerDrawerListener());
    }

    private ListAdapterDelegate createConfiguredAdapter() {
        final ArrayList<AdapterDelegate<List<ListItem>>> delegates = new ArrayList<>();
        final DeviceItemDelegate deviceItemDelegate = new DeviceItemDelegate(getContext());
        deviceItemDelegate.setOnItemClickListener((position, item) -> {
            mRouter.pushController(RouterTransaction.with(new SingleDeviceAppListController(item.getName(), item.getIdentifier())));
            mDrawerLayout.closeDrawer(Gravity.LEFT);
            clearPreviousSelections();
            item.setSelected(true);
            mAdapter.notifyDataSetChanged();
        });
        delegates.add(new EmptyDeviceItemDelegate(getContext()));
        delegates.add(deviceItemDelegate);
        return new ListAdapterDelegate(delegates, null);
    }

    private void clearPreviousSelections() {
        final List<ListItem> items = mAdapter.getItems();
        for (final ListItem item : items) {
            if (item instanceof Device) {
                ((Device) item).setSelected(false);
            }
        }
    }

    private void lockDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void unLockDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void setUpViews() {
        mContainer = (ViewGroup) findViewById(R.id.controllerContainer);
        mMyDevices = (TextView) findViewById(R.id.tvMyDevices);
        mProgressBar = (ProgressBar) findViewById(R.id.pbProgress);
    }

    private void onDevicesLoadSuccess(final List<DeviceDTO> devices) {
        //TODO: refactor this latter
        mRecyclerView.setVisibility(View.VISIBLE);
        mMyDevices.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mAdapter.replaceData(DTOConverter.convertDeviceDTOListToDeviceUiList(devices));
        mAdapter.notifyDataSetChanged();
    }

    private void onDevicesLoadError(final Throwable throwable) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mMyDevices.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        mAdapter.clear();
        mAdapter.addHeadItem(new EmptyDevice());
        mAdapter.notifyDataSetChanged();
    }

    private void configureRecyclerViewInNavDrawer() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rvMyDevices);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = createConfiguredAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }
}