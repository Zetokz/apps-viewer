package ua.zetokz.appsviewer.presentation.app_details;

import android.content.Intent;

import ua.zetokz.appsviewer.manager.ApplicationsManager;
import ua.zetokz.appsviewer.presentation.model.AppInfo;

/**
 * Created by Andrzej Mistetskij on 22.01.17 17:59
 */
public class AppDetailsPresenter implements AppDetailsContract.Presenter {

    private final AppDetailsContract.View mView;
    private final ApplicationsManager mApplicationsManager;
    private final AppInfo mAppInfo;

    public AppDetailsPresenter(final AppDetailsContract.View view, final ApplicationsManager applicationsManager, final String packageName) {
        mView = view;
        mApplicationsManager = applicationsManager;
        mAppInfo = mApplicationsManager.getAppInfoByPackageName(packageName);
    }

    @Override
    public void start() {
        mView.showAppInfo(mAppInfo);
    }

    @Override
    public void onOpenAppMenuItemClicked() {
        final Intent intent = mApplicationsManager.createIntentForOpeningApp(mAppInfo);
        if (intent != null) {
            mView.openApp(intent);
        } else {
            mView.showAppCantBeOpened();
        }
    }

    @Override
    public void onDeleteAppMenuItemClicker() {
        final Intent intent = mApplicationsManager.createIntentForDeletingApp(mAppInfo);
        mView.deleteApp(intent);
    }
}
