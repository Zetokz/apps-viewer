package ua.zetokz.appsviewer.presentation.progress;

import ua.zetokz.appsviewer.presentation.BasePresenter;
import ua.zetokz.appsviewer.presentation.BaseView;

/**
 * Created by Andrzej Mistetskij on 21.01.17 22:41
 */
public interface ProgressContract {

    interface View extends BaseView<Presenter> {

        void setProgressTextVisibility(boolean visible);

        void showProgressText(String pregressText);
    }

    interface Presenter extends BasePresenter {

    }
}
