package ua.zetokz.appsviewer.presentation;

import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

public interface ActionBarProvider {

    ActionBar getSupportActionBar();

    void setSupportActionBar(Toolbar toolbar);
}