package ua.zetokz.appsviewer.adapter;

/**
 * Created by Yevhenii Rechun on 21.01.17 13:33.
 */
public interface OnItemClickListener<T> {

    /**
     * Listener for intercept click in list views
     *
     * @param position Position of view in list
     * @param item     Data dataItem (Model) that complies view
     */
    void onItemClick(int position, T item);
}