package ua.zetokz.appsviewer.adapter;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter;

import java.util.ArrayList;
import java.util.List;

import ua.zetokz.appsviewer.presentation.model.ListItem;

/**
 * Created by Yevhenii Rechun on 21.01.17 14:53.
 */
public class ListAdapterDelegate extends ListDelegationAdapter<List<ListItem>> {

    /**
     * Create new ListAdapterDelegate instance.
     *
     * @param delegates List of delegates that will handle different types of mItems.
     * @param items     List of mItems.
     */
    public ListAdapterDelegate(final List<AdapterDelegate<List<ListItem>>> delegates, final List<ListItem> items) {
        this.items = new ArrayList<>();
        if (items != null) this.items.addAll(items);

        for (final AdapterDelegate<List<ListItem>> delegate : delegates) {
            delegatesManager.addDelegate(delegate);
        }
    }

    /**
     * Add dataItem to the beginning of the list.
     *
     * @param item Recycler view dataItem.
     */
    public <T extends ListItem> void addHeadItem(final T item) {
        items.add(0, item);
    }

    /**
     * Add dataItem to the end of the list.
     *
     * @param item Item.
     */
    public <T extends ListItem> void addTailItem(final T item) {
        items.add(item);
    }

    /**
     * Add the list of mItems to the end of the list.
     *
     * @param item Item.
     */
    public void addTailAllItems(final List<? extends ListItem> item) {
        items.addAll(item);
    }

    public void replaceData(final List<? extends ListItem> items) {
        this.items.clear();
        this.items.addAll(items);
    }

    /**
     * Add items below position
     *  @param position Position
     * @param objects  List of items
     */
    public void addItemsToPosition(final int position, final List<? extends ListItem> objects) {
        items.addAll(position, objects);
    }

    /**
     * Remove items below position
     *
     * @param from  Position
     * @param count Count of dataItem that will be removed
     */
    public void removeItemsFromPosition(final int from, final int count) {
        for (int i = 0; i < count; i++) {
            items.remove(from);
        }
    }

    /**
     * Add dataItem at given position
     *
     * @param position Position
     * @param item     Item
     */
    public void addItemAtPosition(final int position, final ListItem item) {
        items.add(position, item);
    }

    /**
     * Remove dataItem at given position
     *
     * @param position Position
     */
    public void removeItemAtPosition(int position) {
        items.remove(position);
    }

    /**
     * Clear all items in adapter.
     */
    public void clear() {
        items.clear();
    }

}