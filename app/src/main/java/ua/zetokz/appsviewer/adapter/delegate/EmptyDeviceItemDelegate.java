package ua.zetokz.appsviewer.adapter.delegate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.model.EmptyDevice;
import ua.zetokz.appsviewer.presentation.model.ListItem;

/**
 * Created by Andrzej Mistetskij on 23.01.17 02:17
 */
public class EmptyDeviceItemDelegate extends BaseAdapterDelegate<EmptyDeviceItemDelegate.ViewHolder, EmptyDevice> {

    public EmptyDeviceItemDelegate(@NonNull final Context context) {super(context);}

    @Override
    protected ViewHolder createViewHolder(@Nullable final View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected int getItemResId() {
        return R.layout.item_empty_device;
    }

    @Override
    protected boolean isForViewType(@NonNull final ListItem item, @NonNull final List<ListItem> items, final int position) {
        return item instanceof EmptyDevice;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(final View itemView) {
            super(itemView);
        }
    }
}