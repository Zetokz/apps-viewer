package ua.zetokz.appsviewer.adapter.delegate;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.model.Device;
import ua.zetokz.appsviewer.presentation.model.ListItem;

import static android.graphics.Typeface.BOLD;
import static android.graphics.Typeface.NORMAL;

/**
 * Created by Andrzej Mistetskij on 23.01.17 00:58
 */
public class DeviceItemDelegate extends BaseAdapterDelegate<DeviceItemDelegate.ViewHolder, Device> {

    public DeviceItemDelegate(@NonNull final Context context) {
        super(context);
        useClickListenerForRootItem = true;
    }

    @Override
    protected ViewHolder createViewHolder(@Nullable final View itemView) {
        return new ViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(final DeviceItemDelegate.ViewHolder holder, final Device device) {
        holder.bind(device);
    }

    @Override
    protected int getItemResId() {
        return R.layout.item_device_list;
    }

    @Override
    protected boolean isForViewType(@NonNull final ListItem item, @NonNull final List<ListItem> items, final int position) {
        return item instanceof Device;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mDeviceName;

        ViewHolder(final View itemView) {

            super(itemView);
            mDeviceName = (TextView) itemView.findViewById(R.id.tvDeviceName);
        }

        void bind(final Device device) {
            mDeviceName.setText(device.getName());
            mDeviceName.setSelected(device.isSelected());
            mDeviceName.setTypeface(null, device.isSelected() ? BOLD : NORMAL);
        }
    }
}
