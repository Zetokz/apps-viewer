package ua.zetokz.appsviewer.adapter.delegate;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Yevhenii Rechun on 21.01.17 13:39.
 */
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate;

import java.util.List;

import ua.zetokz.appsviewer.adapter.OnItemClickListener;
import ua.zetokz.appsviewer.presentation.model.ListItem;

public abstract class BaseAdapterDelegate<VH extends RecyclerView.ViewHolder, T extends ListItem>
        extends AbsListItemAdapterDelegate<T, ListItem, VH> {

    protected OnItemClickListener<T> mClickListener;
    protected boolean useClickListenerForRootItem = true;
    protected final LayoutInflater mLayoutInflater;
    protected final Context mContext;

    public BaseAdapterDelegate(final Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
    }

    /**
     * Click listener for intercept click on root dataItem in recycler
     *
     * @param clickListener Click listener
     */
    public void setOnItemClickListener(final OnItemClickListener<T> clickListener) {
        mClickListener = clickListener;
    }

    @NonNull
    @Override
    protected VH onCreateViewHolder(final ViewGroup parent) {
        if (getItemResId() != 0) {
            return createViewHolder(mLayoutInflater.inflate(getItemResId(), parent, false));
        } else {
            return createViewHolder(parent);
        }
    }

    @Override
    protected void onBindViewHolder(@NonNull final T item, @NonNull final VH holder, @NonNull final List<Object> payloads) {
        onBindViewHolder(holder, item);
        onBindViewHolder(holder, item, mClickListener);
        if (mClickListener != null && useClickListenerForRootItem) {
            holder.itemView.setOnClickListener(v -> {
                mClickListener.onItemClick(holder.getAdapterPosition(), item);
            });
        }
    }

    protected void onBindViewHolder(final VH holder, final T item) {
        //for child implementations
    }

    protected void onBindViewHolder(final VH holder, final T item, final OnItemClickListener<T> clickListener) {
        //for child implementations
    }

    protected abstract VH createViewHolder(@Nullable final View itemView);

    @LayoutRes
    protected abstract int getItemResId();

}