package ua.zetokz.appsviewer.adapter.delegate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.model.ListItem;
import ua.zetokz.appsviewer.presentation.model.ServerApp;
import ua.zetokz.appsviewer.util.ResUtil;

/**
 * Created by Yevhenii Rechun on 23.01.17 01:07.
 */
public class RemoteDeviceAppItemDelegate extends BaseAdapterDelegate<RemoteDeviceAppItemDelegate.ViewHolder, ServerApp> {

    public RemoteDeviceAppItemDelegate(@NonNull final Context context) {
        super(context);
        useClickListenerForRootItem = true;
    }

    @Override
    protected RemoteDeviceAppItemDelegate.ViewHolder createViewHolder(@Nullable final View itemView) {
        return new RemoteDeviceAppItemDelegate.ViewHolder(itemView);
    }

    @Override
    protected void onBindViewHolder(final RemoteDeviceAppItemDelegate.ViewHolder holder, final ServerApp item) {
        holder.bind(item);
    }

    @Override
    protected int getItemResId() {
        return R.layout.item_app_from_server;
    }

    @Override
    protected boolean isForViewType(@NonNull final ListItem item, @NonNull final List<ListItem> items, final int position) {
        return item instanceof ServerApp;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mAppName;
        private final TextView mPackageName;
        private final TextView mInstalledDate;
        private final TextView mUpdatedDate;
        private final TextView mAppStatus;
        private final ImageView mIcon;

        ViewHolder(final View itemView) {
            super(itemView);

            mAppName = (TextView) itemView.findViewById(R.id.tvAppName);
            mPackageName = (TextView) itemView.findViewById(R.id.tvPackageName);
            mInstalledDate = (TextView) itemView.findViewById(R.id.tvInstalledDate);
            mUpdatedDate = (TextView) itemView.findViewById(R.id.tvUpdatedDate);
            mAppStatus = (TextView) itemView.findViewById(R.id.tvAppStatus);
            mIcon = (ImageView) itemView.findViewById(R.id.ivAppAvatar);
        }

        void bind(final ServerApp serverApp) {
            mAppName.setText(serverApp.getName());
            mPackageName.setText(serverApp.getPackageName());
            mInstalledDate.setText(getContext().getString(R.string.installed_date, convertAppInstalledDate(serverApp.getInstalledDate())));
            mUpdatedDate.setText(getContext().getString(R.string.updated_date, convertAppInstalledDate(serverApp.getUpdatedDate())));
            // TODO: 23.01.17 mIcon.setIcon()
            mAppStatus.setText(serverApp.isDeleted() ? R.string.deleted : R.string.installed);
            mAppStatus.setAllCaps(true);
            mAppStatus.setTextColor(ResUtil.getColor(getContext(), serverApp.isDeleted() ? R.color.red : R.color.green));
        }

        public Context getContext() {
            return itemView.getContext();
        }

        // TODO: 23.01.17 move this method to singleton device util with correct time zone
        private static String convertAppInstalledDate(final long dateLong) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm");
            final Date date = new Date(dateLong);
            return sdf.format(date);
        }

    }
}
