package ua.zetokz.appsviewer.adapter.delegate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ua.zetokz.appsviewer.R;
import ua.zetokz.appsviewer.adapter.OnItemClickListener;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.presentation.model.ListItem;

/**
 * Created by Yevhenii Rechun on 21.01.17 14:35.
 */
public class AppSingleItemDelegate extends BaseAdapterDelegate<AppSingleItemDelegate.ViewHolder, AppInfo> {

    private final OnItemClickListener<AppInfo> mOpenAppClickListener;
    private final OnItemClickListener<AppInfo> mRootItemClickListener;

    public AppSingleItemDelegate(@NonNull final Context context,
                                 @NonNull final OnItemClickListener<AppInfo> onRootItemClickListener,
                                 @NonNull final OnItemClickListener<AppInfo> onOpenAppClickListener) {
        super(context);
        mOpenAppClickListener = onOpenAppClickListener;
        mRootItemClickListener = onRootItemClickListener;
    }

    @Override
    protected ViewHolder createViewHolder(@Nullable final View itemView) {
        return new ViewHolder(itemView, mOpenAppClickListener, mRootItemClickListener);
    }

    @Override
    protected void onBindViewHolder(final ViewHolder holder, final AppInfo item) {
        holder.bind(item);
    }

    @Override
    protected int getItemResId() {
        return R.layout.item_app_list;
    }

    @Override
    protected boolean isForViewType(@NonNull final ListItem item, @NonNull final List<ListItem> items, final int position) {
        return item instanceof AppInfo;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final OnItemClickListener<AppInfo> mOpenAppClickListener;
        private final OnItemClickListener<AppInfo> mRootItemClickListener;

        private final TextView mAppName;
        private final TextView mOpenApp;
        private final TextView mPackageName;
        private final TextView mSize;
        private final ImageView mIcon;

        ViewHolder(final View itemView,
                   final OnItemClickListener<AppInfo> openAppClickListener,
                   final OnItemClickListener<AppInfo> rootItemClickListener) {

            super(itemView);
            mAppName = (TextView) itemView.findViewById(R.id.tvAppName);
            mPackageName = (TextView) itemView.findViewById(R.id.tvPackageName);
            mOpenApp = (TextView) itemView.findViewById(R.id.tvOpenApp);
            mIcon = (ImageView) itemView.findViewById(R.id.ivAppAvatar);
            mSize = (TextView) itemView.findViewById(R.id.tvSize);
            mOpenAppClickListener = openAppClickListener;
            mRootItemClickListener = rootItemClickListener;
        }

        void bind(final AppInfo appInfo) {
            mIcon.setImageDrawable(appInfo.getIcon());
            mAppName.setText(appInfo.getAppName());
            mSize.setText(appInfo.getApplicationSize());
            mPackageName.setText(appInfo.getPackageName());

            mOpenApp.setOnClickListener(__ -> mOpenAppClickListener.onItemClick(getAdapterPosition(), appInfo));
            itemView.setOnClickListener(__ -> mRootItemClickListener.onItemClick(getAdapterPosition(), appInfo));
        }

    }
}
