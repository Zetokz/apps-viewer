package ua.zetokz.appsviewer.domain.converter;

import java.util.List;

import rx.Observable;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.presentation.model.Device;
import ua.zetokz.appsviewer.presentation.model.ServerApp;

/**
 * Created by Andrzej Mistetskij on 23.01.17 02:06
 */
public class DTOConverter {

    public static List<Device> convertDeviceDTOListToDeviceUiList(final List<DeviceDTO> deviceDTOs) {
        return Observable.from(deviceDTOs)
                .map(DTOConverter::convertDeviceDTOtoDeviceUiItem)
                .toList()
                .toBlocking()
                .first();
    }

    public static Device convertDeviceDTOtoDeviceUiItem(final DeviceDTO deviceDTO) {
        return new Device(deviceDTO.getName(), deviceDTO.getIdentifier());
    }

    public static List<ServerApp> convertApplicationDTOlistToServerAppList(final List<ApplicationDTO> applications) {
        return Observable.from(applications)
                .map(DTOConverter::convertApplicationDTOtoServerApp)
                .toSortedList((serverApp1, serverApp2) -> serverApp1.getName().compareTo(serverApp2.getName()))
                .toBlocking()
                .first();
    }

    public static ServerApp convertApplicationDTOtoServerApp(final ApplicationDTO application) {
        return new ServerApp(
                application.getName(), application.getPackageName(), application.getIcon(),
                application.getInstalledDate(), application.getUpdatedDate(), application.isDeleted()
        );
    }
}
