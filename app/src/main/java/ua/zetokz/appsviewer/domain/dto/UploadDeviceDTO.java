package ua.zetokz.appsviewer.domain.dto;

import com.google.gson.annotations.SerializedName;

import ua.zetokz.appsviewer.util.DeviceUtil;

public class UploadDeviceDTO {

    @SerializedName("credentials")
    private AccountDTO credentials;
    @SerializedName("device")
    private DeviceDTO device;

    public UploadDeviceDTO() {
        device = DeviceUtil.createCurrentDevice();
    }

    public AccountDTO getCredentials() {
        return credentials;
    }

    public void setCredentials(final AccountDTO credentials) {
        this.credentials = credentials;
    }

    public DeviceDTO getDevice() {
        return device;
    }
}