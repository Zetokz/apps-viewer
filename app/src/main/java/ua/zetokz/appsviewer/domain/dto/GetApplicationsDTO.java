package ua.zetokz.appsviewer.domain.dto;

/**
 * Created by Yevhenii Rechun on 23.01.17 02:40.
 */
public class GetApplicationsDTO {

    private AccountDTO credentials;
    private String deviceIdentifier;

    public GetApplicationsDTO(final AccountDTO credentials, final String deviceIdentifier) {
        this.credentials = credentials;
        this.deviceIdentifier = deviceIdentifier;
    }

    public AccountDTO getCredentials() {
        return credentials;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }
}
