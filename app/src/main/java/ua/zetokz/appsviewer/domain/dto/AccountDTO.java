package ua.zetokz.appsviewer.domain.dto;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:26
 */
public class AccountDTO {

    private final String username;
    private final String password;

    public AccountDTO(final String name, final String password) {
        this.username = name;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
