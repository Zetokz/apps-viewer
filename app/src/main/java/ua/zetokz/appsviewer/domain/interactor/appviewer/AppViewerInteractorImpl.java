package ua.zetokz.appsviewer.domain.interactor.appviewer;

import java.util.List;

import rx.Scheduler;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.interactor.BaseInteractorImpl;
import ua.zetokz.appsviewer.domain.interactor.UseCase;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.dto.UploadDeviceDTO;
import ua.zetokz.appsviewer.presentation.model.AppInfo;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:35
 */
public class AppViewerInteractorImpl extends BaseInteractorImpl implements AppViewerInteractor {

    /**
     * @param executionThread     Worked scheduler.
     * @param postExecutionThread After request scheduler.
     */
    public AppViewerInteractorImpl(final Scheduler executionThread, final Scheduler postExecutionThread) {
        super(executionThread, postExecutionThread);
    }

    @Override
    public UseCase<AccountDTO, Throwable> login(final String name, final String password) {
        return cacheUseCaseAndReturn(new SignInUseCase(mExecutionThread, mPostExecutionThread, new AccountDTO(name, password)));
    }

    @Override
    public UseCase<DeviceDTO, Throwable> uploadDevice(final UploadDeviceDTO uploadDeviceDTO) {
        return cacheUseCaseAndReturn(new AddDeviceUseCase(mExecutionThread, mPostExecutionThread, uploadDeviceDTO));
    }

    @Override
    public UseCase<List<ApplicationDTO>, Throwable> uploadApplications(final AccountDTO credentials,
                                                                       final String deviceIdentifier,
                                                                       final List<AppInfo> apps) {

        return cacheUseCaseAndReturn(new AddApplicationsUseCase(mExecutionThread, mPostExecutionThread,
                credentials, deviceIdentifier, apps));
    }

    @Override
    public UseCase<List<DeviceDTO>, Throwable> getDevices(final AccountDTO credentials) {
        return cacheUseCaseAndReturn(new GetDevicesUseCase(mExecutionThread, mPostExecutionThread, credentials));
    }

    @Override
    public UseCase<List<ApplicationDTO>, Throwable> loadDeviceApplications(final AccountDTO credentials,
                                                                           final String deviceIdentifier) {

        return cacheUseCaseAndReturn(new LoadSingeDeviceApplicationsUseCase(mExecutionThread, mPostExecutionThread,
                credentials, deviceIdentifier));
    }
}
