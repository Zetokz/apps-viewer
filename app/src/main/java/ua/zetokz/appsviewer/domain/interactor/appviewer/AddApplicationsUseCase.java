package ua.zetokz.appsviewer.domain.interactor.appviewer;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Scheduler;
import ua.zetokz.appsviewer.domain.RestApiManager;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.UploadApplicationsDTO;
import ua.zetokz.appsviewer.domain.interactor.UseCase;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.presentation.model.AppInfo;
import ua.zetokz.appsviewer.util.ImageUtil;

/**
 * Created by Yevhenii Rechun on 22.01.17 22:12.
 */
class AddApplicationsUseCase extends UseCase<List<ApplicationDTO>, Throwable> {

    private final List<AppInfo> mAppInfos;
    private final AccountDTO mAccount;
    private final String mDeviceIdentifier;

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     */
    protected AddApplicationsUseCase(final Scheduler executionThread, final Scheduler postExecutionThread,
                                     final AccountDTO account, final String deviceIdentifier, final List<AppInfo> appInfos) {

        super(executionThread, postExecutionThread);
        mAppInfos = appInfos;
        mAccount = account;
        mDeviceIdentifier = deviceIdentifier;
    }

    @Override
    protected Observable<List<ApplicationDTO>> buildUseCaseObservable() {
        final List<ApplicationDTO> applicationDTOs = convertAppsListForServer(mAppInfos);
        final UploadApplicationsDTO uploadApplicationsDTO =
                new UploadApplicationsDTO(mAccount, mDeviceIdentifier, applicationDTOs);

        return RestApiManager.getAppViewerService().uploadApplications(uploadApplicationsDTO);
    }

    private ApplicationDTO convertAppInfoForServer(final AppInfo appInfo) {
//        final String encodedIcon = convertDrawableToString(appInfo.getIcon());
        return new ApplicationDTO.Builder()
                .name(appInfo.getAppName())
                .packageName(appInfo.getPackageName())
//                .icon(encodedIcon)
                .installedDate(appInfo.getInstalledDate())
                .updatedDate(appInfo.getUpdatedDate())
                .build();
    }

    private String convertDrawableToString(final Drawable icon) {
        String encodedIcon = "";
        if (BitmapDrawable.class.isInstance(icon)) {
            final Bitmap bitmap = ((BitmapDrawable) icon).getBitmap();
            encodedIcon = ImageUtil.encodeBitmap(bitmap);
        }
        return encodedIcon;
    }

    private List<ApplicationDTO> convertAppsListForServer(final List<AppInfo> appInfos) {
        final ArrayList<ApplicationDTO> applicationDTOs = new ArrayList<>();
        for (final AppInfo appInfo : appInfos) {
            applicationDTOs.add(convertAppInfoForServer(appInfo));
        }
        return applicationDTOs;
    }

}
