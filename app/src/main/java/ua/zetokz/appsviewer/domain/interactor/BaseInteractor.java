package ua.zetokz.appsviewer.domain.interactor;

/**
 * Created by Yevhenii Rechun on 21.01.17
 */


public interface BaseInteractor {

    /**
     * Unsubscribe from all cached subscriptions.
     */
    void unsubscribe();
}
