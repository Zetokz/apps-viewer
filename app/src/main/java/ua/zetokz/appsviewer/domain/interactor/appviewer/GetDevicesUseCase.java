package ua.zetokz.appsviewer.domain.interactor.appviewer;

import java.util.List;

import rx.Observable;
import rx.Scheduler;
import ua.zetokz.appsviewer.domain.RestApiManager;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.interactor.UseCase;

/**
 * Created by Andrzej Mistetskij on 23.01.17 01:44
 */
public class GetDevicesUseCase extends UseCase<List<DeviceDTO>, Throwable> {
    private final AccountDTO mAccountDTO;

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     * @param accountDTO
     */
    protected GetDevicesUseCase(final Scheduler executionThread, final Scheduler postExecutionThread, final AccountDTO accountDTO) {
        super(executionThread, postExecutionThread);
        mAccountDTO = accountDTO;
    }

    @Override
    protected Observable<List<DeviceDTO>> buildUseCaseObservable() {
        return RestApiManager.getAppViewerService().getDevices(mAccountDTO);
    }
}
