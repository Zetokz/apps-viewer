package ua.zetokz.appsviewer.domain.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yevhenii Rechun on 22.01.17 22:41.
 */
public class UploadApplicationsDTO {

    @SerializedName("credentials")
    private AccountDTO accountDTO;
    @SerializedName("deviceIdentifier")
    private String deviceIdentifier;
    @SerializedName("applications")
    private List<ApplicationDTO> applicationDTOs;

    public UploadApplicationsDTO(final AccountDTO accountDTO, final String deviceIdentifier, final List<ApplicationDTO> applicationDTOs) {
        this.accountDTO = accountDTO;
        this.deviceIdentifier = deviceIdentifier;
        this.applicationDTOs = applicationDTOs;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public List<ApplicationDTO> getApplicationDTOs() {
        return applicationDTOs;
    }
}
