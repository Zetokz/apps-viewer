package ua.zetokz.appsviewer.domain.interactor.appviewer;

import rx.Observable;
import rx.Scheduler;
import ua.zetokz.appsviewer.domain.RestApiManager;
import ua.zetokz.appsviewer.domain.interactor.UseCase;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.dto.UploadDeviceDTO;

/**
 * Created by Andrzej Mistetskij on 22.01.17 14:40
 */
class AddDeviceUseCase extends UseCase<DeviceDTO, Throwable> {

    private final UploadDeviceDTO mUploadDeviceDTO;

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     * @param uploadDeviceDTO
     */
    protected AddDeviceUseCase(final Scheduler executionThread, final Scheduler postExecutionThread, final UploadDeviceDTO uploadDeviceDTO) {
        super(executionThread, postExecutionThread);
        mUploadDeviceDTO = uploadDeviceDTO;
    }

    @Override
    protected Observable<DeviceDTO> buildUseCaseObservable() {
        return RestApiManager.getAppViewerService().addDevice(mUploadDeviceDTO);
    }
}
