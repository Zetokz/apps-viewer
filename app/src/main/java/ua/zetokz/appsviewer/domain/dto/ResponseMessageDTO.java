package ua.zetokz.appsviewer.domain.dto;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:32
 */
public class ResponseMessageDTO {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
