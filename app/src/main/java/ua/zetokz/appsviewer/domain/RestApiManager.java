package ua.zetokz.appsviewer.domain;

import ua.zetokz.appsviewer.domain.restservice.AppViewerService;
import ua.zetokz.appsviewer.domain.restservice.ServiceGenerator;

/**
 * Created by Andrzej Mistetskij on 21.01.17 21:52
 */
public abstract class RestApiManager {
    private static final AppViewerService sAppViewerService = ServiceGenerator.createService(AppViewerService.class);

    public static AppViewerService getAppViewerService() {
        return sAppViewerService;
    }
}
