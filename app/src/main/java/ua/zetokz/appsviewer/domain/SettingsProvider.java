package ua.zetokz.appsviewer.domain;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

import ua.zetokz.appsviewer.util.Constants;

/**
 * Created by Andrzej Mistetskij on 21.01.17 21:31
 */
public class SettingsProvider {

    private static AppConfiguration sAppConfiguration;

    /**
     * Load configuration from given InputStream.
     *
     * @param configuration Given InputStream.
     * @throws IOException
     */
    public static void loadConfiguration(InputStream configuration) throws IOException {
        final String jsonString = IOUtils.toString(configuration, Constants.UTF8);
        final Gson gson = new Gson();
        sAppConfiguration = gson.fromJson(jsonString, AppConfiguration.class);
    }

    /**
     * Get Application Viewer configuration
     *
     * @return AppConfiguration
     */
    public static AppConfiguration getAppConfiguration() {
        return sAppConfiguration;
    }

    /**
     * Get base url.
     *
     * @return Base url.
     */
    public static String getApiBaseUrl() {
        return sAppConfiguration.getBaseUrl();
    }
}
