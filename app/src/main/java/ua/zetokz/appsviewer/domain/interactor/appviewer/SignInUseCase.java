package ua.zetokz.appsviewer.domain.interactor.appviewer;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;
import ua.zetokz.appsviewer.domain.RestApiManager;
import ua.zetokz.appsviewer.domain.interactor.UseCase;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.ResponseMessageDTO;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:36
 */
class SignInUseCase extends UseCase<AccountDTO, Throwable> {

    private final AccountDTO mAccountDTO;

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     * @param accountDTO
     */
    protected SignInUseCase(final Scheduler executionThread, final Scheduler postExecutionThread, final AccountDTO accountDTO) {
        super(executionThread, postExecutionThread);
        mAccountDTO = accountDTO;
    }

    @Override
    protected Observable<AccountDTO> buildUseCaseObservable() {
        return RestApiManager.getAppViewerService()
                .login(mAccountDTO)
                .map(responseMessageDTO -> mAccountDTO);
    }
}
