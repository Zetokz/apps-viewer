package ua.zetokz.appsviewer.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Andrzej Mistetskij on 21.01.17 21:25
 */
@Parcel(Parcel.Serialization.BEAN)
public class AppConfiguration {
    @SerializedName("baseUrl")
    private  String mBaseUrl;

    /**
     * Create empty AppConfiguration instance
     */
    public AppConfiguration() {
        mBaseUrl = null;
    }

    /**
     * Get BaseUrl.
     *
     * @return BaseUrl.
     */
    public String getBaseUrl() {
        return mBaseUrl;
    }

    /**
     * Set BaseUrl
     * @param baseUrl
     */
    public void setBaseUrl(final String baseUrl) {
        mBaseUrl = baseUrl;
    }
}
