package ua.zetokz.appsviewer.domain.interactor.appviewer;

import java.util.List;

import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.dto.UploadDeviceDTO;
import ua.zetokz.appsviewer.domain.interactor.BaseInteractor;
import ua.zetokz.appsviewer.domain.interactor.UseCase;
import ua.zetokz.appsviewer.presentation.model.AppInfo;

/**
 * Created by Andrzej Mistetskij on 22.01.17 00:30
 */
public interface AppViewerInteractor extends BaseInteractor {

    UseCase<AccountDTO, Throwable> login(String name, String password);

    UseCase<DeviceDTO, Throwable> uploadDevice(UploadDeviceDTO uploadDeviceDTO);

    UseCase<List<ApplicationDTO>, Throwable> uploadApplications(AccountDTO credentials, String deviceIdentifier, List<AppInfo> apps);

    UseCase<List<DeviceDTO>, Throwable> getDevices(AccountDTO credentials);

    UseCase<List<ApplicationDTO>, Throwable> loadDeviceApplications(AccountDTO credentials, String deviceIdentifier);
}
