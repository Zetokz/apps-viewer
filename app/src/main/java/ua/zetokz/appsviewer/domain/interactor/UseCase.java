package ua.zetokz.appsviewer.domain.interactor;

import android.util.Log;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.subscriptions.Subscriptions;

/**
 * Created by Yevhenii Rechun on 21.01.17
 */

public abstract class UseCase<T, E extends Throwable> {

    private static final String TAG = UseCase.class.getSimpleName();

    /**
     * Wrapped error action for using method reference. {@link Action1<Throwable>}
     */
    public interface ErrorAction<E> {
        void call(E error);
    }

    private final Scheduler mExecutionThread;
    private final Scheduler mPostExecutionThread;

    private Action1<T> mNextAction;
    private Action0 mCompleteAction;
    private ErrorAction<E> mErrorAction;

    private Subscription mSubscription = Subscriptions.empty();
    private final Subscriber<T> wrappedSubscriber = new Subscriber<T>() {
        @Override
        public void onCompleted() {
            if (mCompleteAction != null) {
                mCompleteAction.call();
            }
        }

        @Override
        public void onError(final Throwable e) {
            if (mErrorAction != null) {
                mErrorAction.call(wrapErrorIfNeeded(e));
            }
        }

        @Override
        public void onNext(final T t) {
            if (mNextAction != null) {
                mNextAction.call(t);
            }
        }
    };

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     */
    protected UseCase(final Scheduler executionThread, final Scheduler postExecutionThread) {
        mExecutionThread = executionThread;
        mPostExecutionThread = postExecutionThread;
    }

    protected E wrapErrorIfNeeded(final Throwable throwable) {
        try {
            return (E) throwable;
        } catch (ClassCastException ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
            throw new ClassCastException("If type E is not Throwable. You should override this method");
        }
    }

    /**
     * Builds an {@link rx.Observable} which will be used when executing the current {@link UseCase}.
     */
    protected abstract Observable<T> buildUseCaseObservable();

    /**
     * Executes the current use case.
     *
     * @param useCaseSubscriber The guy who will be listen to the observable build
     *                          with {@link #buildUseCaseObservable()}.
     */
    public void execute(final Subscriber<T> useCaseSubscriber) {
        mCompleteAction = useCaseSubscriber::onCompleted;
        mNextAction = useCaseSubscriber::onNext;
        mErrorAction = useCaseSubscriber::onError;
        subscribe();
    }

    /**
     * Executes the current use case. onComplete action and onError are ignored.
     *
     * @param nextAction Next action with <T> expected result
     */
    public void execute(final Action1<T> nextAction) {
        mNextAction = nextAction;
        subscribe();
    }

    /**
     * Executes the current use case. onComplete action is ignored.
     *
     * @param nextAction  Next action with <T> expected result
     * @param errorAction Error action.
     */
    public void execute(final Action1<T> nextAction, final ErrorAction<E> errorAction) {
        mNextAction = nextAction;
        mErrorAction = errorAction;
        subscribe();
    }

    /**
     * Executes the current use case. onNext action is ignored.
     *
     * @param completeAction Complete action.
     * @param errorAction    Error action.
     */
    public void execute(final Action0 completeAction, final ErrorAction<E> errorAction) {
        mCompleteAction = completeAction;
        mErrorAction = errorAction;
        subscribe();
    }

    /**
     * Executes the current use case.
     *
     * @param nextAction     Next action with expected result object.
     * @param errorAction    Error action.
     * @param completeAction Complete action.
     */
    public void execute(final Action1<T> nextAction, final ErrorAction<E> errorAction, final Action0 completeAction) {
        mNextAction = nextAction;
        mErrorAction = errorAction;
        mCompleteAction = completeAction;
        subscribe();
    }

    /**
     * Executes the current use case without callbacks.
     */
    public void execute() {
        subscribe();
    }

    private Observable<T> buildUseCaseWithSchedulers() {
        return buildUseCaseObservable()
//                .doOnUnsubscribe(this::notifySubscribptionCanceledIfNeeded)
                .subscribeOn(mExecutionThread)
                .observeOn(mPostExecutionThread);
    }

    private void notifySubscribptionCanceledIfNeeded() {
        if (!mSubscription.isUnsubscribed()) {
            wrappedSubscriber.onError(new Throwable("Canceled"));
        }
    }

    private void subscribe() {
        mSubscription = buildUseCaseWithSchedulers().subscribe(wrappedSubscriber);
    }

    /**
     * Unsubscribes from current {@link rx.Subscription}.
     */
    void unsubscribe() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }
}