package ua.zetokz.appsviewer.domain.dto;

/**
 * Created by Andrzej Mistetskij on 22.01.17 14:35
 */
public class DeviceDTO {

    private Long id;
    private String name;
    private String identifier;

    public DeviceDTO() {}

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }
}
