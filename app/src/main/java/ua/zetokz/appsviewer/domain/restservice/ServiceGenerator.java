package ua.zetokz.appsviewer.domain.restservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ua.zetokz.appsviewer.BuildConfig;
import ua.zetokz.appsviewer.domain.SettingsProvider;

/**
 * Created by Andrzej Mistetskij on 21.01.17 21:42
 */
public class ServiceGenerator {

    public static final String API_BASE_URL = SettingsProvider.getApiBaseUrl();

    public static <S> S createService(Class<S> serviceClass) {
        final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            configureLogsInterceptors(httpClientBuilder);
        }

        final OkHttpClient okHttpClient = httpClientBuilder.build();
        final Gson gson = new GsonBuilder().create();

        final Retrofit retrofitBuilder = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        return retrofitBuilder.create(serviceClass);
    }

    /**
     * Sets interceptors to log api calls
     *
     * @param httpClientBuilder
     */
    private static void configureLogsInterceptors(final OkHttpClient.Builder httpClientBuilder) {
        final HttpLoggingInterceptor loggingBODY = new HttpLoggingInterceptor();
        loggingBODY.setLevel(HttpLoggingInterceptor.Level.BODY);

        final HttpLoggingInterceptor loggingHEADERS = new HttpLoggingInterceptor();
        loggingHEADERS.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        httpClientBuilder.addInterceptor(loggingBODY);
        httpClientBuilder.addInterceptor(loggingHEADERS);
    }
}
