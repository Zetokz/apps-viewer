package ua.zetokz.appsviewer.domain.restservice;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.dto.DeviceDTO;
import ua.zetokz.appsviewer.domain.dto.GetApplicationsDTO;
import ua.zetokz.appsviewer.domain.dto.ResponseMessageDTO;
import ua.zetokz.appsviewer.domain.dto.UploadApplicationsDTO;
import ua.zetokz.appsviewer.domain.dto.UploadDeviceDTO;

/**
 * Created by Andrzej Mistetskij on 21.01.17 21:47
 */
public interface AppViewerService {

    String API = "api/";

    @POST(API + "login")
    Observable<ResponseMessageDTO> login(@Body AccountDTO accountDTO);

    @PUT(API + "device")
    Observable<DeviceDTO> addDevice(@Body UploadDeviceDTO uploadDeviceDTO);

    @PUT(API + "applications")
    Observable<List<ApplicationDTO>> uploadApplications(@Body UploadApplicationsDTO uploadApplicationsDTO);

    @POST(API + "devices")
    Observable<List<DeviceDTO>> getDevices(@Body AccountDTO accountDTO);

    @POST(API + "applications")
    Observable<List<ApplicationDTO>> loadSingleDeviceApplications(@Body GetApplicationsDTO getApplicationsDTO);
}

