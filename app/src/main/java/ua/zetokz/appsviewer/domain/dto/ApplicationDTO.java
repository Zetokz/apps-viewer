package ua.zetokz.appsviewer.domain.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Yevhenii Rechun on 22.01.17 22:13.
 */
public class ApplicationDTO {

    private Long id;
    private String name;
    private String packageName;
    private String icon;
    private long installedDate;
    private long updatedDate;
    private boolean deleted;

    private ApplicationDTO(final String name, final String packageName, final String icon, final long installedDate, final long updatedDate) {
        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
        this.installedDate = installedDate;
        this.updatedDate = updatedDate;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getIcon() {
        return icon;
    }

    public long getInstalledDate() {
        return installedDate;
    }

    public long getUpdatedDate() {
        return updatedDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public static class Builder {

        private String name;
        private String icon;
        private String packageName;
        private long installedDate;
        private long updatedDate;

        public Builder name(final String name) {
            this.name = name;
            return this;
        }

        public Builder packageName(final String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder icon(final String icon) {
            this.icon = icon;
            return this;
        }

        public Builder installedDate(final long installedDate) {
            this.installedDate = installedDate;
            return this;
        }

        public Builder updatedDate(final long updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public ApplicationDTO build() {
            return new ApplicationDTO(name, packageName, icon, installedDate, updatedDate);
        }

    }
}
