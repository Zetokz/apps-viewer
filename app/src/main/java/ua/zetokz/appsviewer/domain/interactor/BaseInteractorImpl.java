package ua.zetokz.appsviewer.domain.interactor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rx.Scheduler;

/**
 * Created by Yevhenii Rechun on 21.01.17
 */


public abstract class BaseInteractorImpl implements BaseInteractor {

    protected final Scheduler mExecutionThread;
    protected final Scheduler mPostExecutionThread;
    private final List<UseCase> mUseCases;

    /**
     *
     * @param executionThread Worked scheduler.
     * @param postExecutionThread After request scheduler.
     */
    public BaseInteractorImpl(final Scheduler executionThread, final Scheduler postExecutionThread) {
        mExecutionThread = executionThread;
        mPostExecutionThread = postExecutionThread;
        mUseCases = new ArrayList<>();
    }

    @Override
    public void unsubscribe() {
        for (Iterator<UseCase> i = mUseCases.iterator(); i.hasNext();) {
            i.next().unsubscribe();
            i.remove();
        }
    }

    protected <U extends UseCase> U cacheUseCaseAndReturn(final U useCase) {
        mUseCases.add(useCase);
        return useCase;
    }
}