package ua.zetokz.appsviewer.domain.interactor.appviewer;

import java.util.List;

import rx.Observable;
import rx.Scheduler;
import ua.zetokz.appsviewer.domain.RestApiManager;
import ua.zetokz.appsviewer.domain.dto.AccountDTO;
import ua.zetokz.appsviewer.domain.dto.ApplicationDTO;
import ua.zetokz.appsviewer.domain.dto.GetApplicationsDTO;
import ua.zetokz.appsviewer.domain.interactor.UseCase;

/**
 * Created by Yevhenii Rechun on 23.01.17 02:35.
 */
class LoadSingeDeviceApplicationsUseCase extends UseCase<List<ApplicationDTO>, Throwable> {

    private final GetApplicationsDTO mGetApplicationsDTO;

    /**
     * @param executionThread     SubscribeOn thread.
     * @param postExecutionThread ObserveOn thread.
     */
    protected LoadSingeDeviceApplicationsUseCase(final Scheduler executionThread, final Scheduler postExecutionThread,
                                                 final AccountDTO credentials, final String deviceIdentifier) {

        super(executionThread, postExecutionThread);
        mGetApplicationsDTO = new GetApplicationsDTO(credentials, deviceIdentifier);
    }

    @Override
    protected Observable<List<ApplicationDTO>> buildUseCaseObservable() {
        return RestApiManager.getAppViewerService().loadSingleDeviceApplications(mGetApplicationsDTO);
    }
}
